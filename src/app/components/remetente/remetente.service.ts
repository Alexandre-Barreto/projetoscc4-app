import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Remetente} from "./remetente.model";
import {Observable} from "rxjs";
import {MatSnackBar} from "@angular/material/snack-bar";

@Injectable({
  providedIn: 'root'
})
export class RemetenteService{
  constructor(private http: HttpClient, private snackBar: MatSnackBar) {}

  showOnConsole(msg: string): void{
    console.log(msg);
  }

  create(remetente: Remetente):Observable<Remetente>{
    return this.http.post<Remetente>("http://localhost:8080/remetentes/save",remetente);
  }

  read(): Observable<Remetente[]>{
    return this.http.get<Remetente[]>("http://localhost:8080/remetentes/list");
  }

  readById(id: number): Observable<Remetente>{
    return this.http.get<Remetente>("http://localhost:8080/remetentes/list/"+id+"?id="+id);
  }

  update(remetente: Remetente): Observable<Remetente>{
    return this.http.put<Remetente>("http://localhost:8080/remetentes/update/"+remetente.idRemetente+"?id="+remetente.idRemetente,remetente);
  }

  delete(id: number): Observable<Remetente>{
    return this.http.delete<Remetente>("http://localhost:8080/remetentes/delete/"+id+"?id="+id);
  }

}
