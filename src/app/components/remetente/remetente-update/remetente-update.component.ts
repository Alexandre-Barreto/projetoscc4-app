import { Component, OnInit } from '@angular/core';
import {RemetenteService} from "../remetente.service";
import {ActivatedRoute, Router} from "@angular/router";
import {Remetente} from "../remetente.model";

@Component({
  selector: 'app-remetente-update',
  templateUrl: './remetente-update.component.html',
  styleUrls: ['./remetente-update.component.css']
})
export class RemetenteUpdateComponent implements OnInit {

  remetente: Remetente

  constructor(private remetenteService: RemetenteService,private router: Router,private route: ActivatedRoute) { }

  ngOnInit(): void {
    const id = +this.route.snapshot.paramMap.get("id");
    this.remetenteService.readById(id).subscribe((remetentes) => {
      this.remetente = remetentes;
  })
  }

  updateRemetente(): void {
    this.remetenteService.update(this.remetente).subscribe(() => {
    });
  }
}

