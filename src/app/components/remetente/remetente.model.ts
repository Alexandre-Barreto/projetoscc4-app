export interface Remetente {
    idRemetente?: number;
    nome: string;
    telefone: number;
    cpf: number;
    endereco: string;
    email: string;

}