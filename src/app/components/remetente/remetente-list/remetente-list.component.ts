import { Component, OnInit } from '@angular/core';
import {Remetente} from "../remetente.model";
import {RemetenteService} from "../remetente.service";

@Component({
  selector: 'app-remetente-list',
  templateUrl: './remetente-list.component.html',
  styleUrls: ['./remetente-list.component.css']
})
export class RemetenteListComponent implements OnInit {


  remetente: Remetente[];
  displayedColumns = ['name','telefone','cpf','endereco','email', 'action']

  constructor(private remetenteService: RemetenteService) { }

  ngOnInit(): void {
    this.remetenteService.read().subscribe(remetentes => {
      this.remetente = remetentes;
    })
  }

  deleteRemetente(id: number): void{
    this.remetenteService.delete(id).subscribe(()=>{
    });
  }


}
