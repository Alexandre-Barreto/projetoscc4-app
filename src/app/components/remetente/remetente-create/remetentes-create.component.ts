import { Component, OnInit } from '@angular/core';
import {RemetenteService} from '../remetente.service'
import {Remetente} from "../remetente.model";
import {Router} from "@angular/router";

@Component({
  selector: 'app-remetentes-create',
  templateUrl: './remetentes-create.component.html',
  styleUrls: ['./remetentes-create.component.css']
})
export class RemetentesCreateComponent implements OnInit {

  constructor(private remetenteService: RemetenteService, private router: Router) { }
  remetente: Remetente ={
    nome:'',
    telefone:null,
    cpf:null,
    endereco:'',
    email:''
  }
  ngOnInit(): void {
  }
  createRemetente(): void{
    this.remetenteService.create(this.remetente).subscribe(()=>{
    });
  }
}


