import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {RemetentesCreateComponent} from "./components/remetente/remetente-create/remetentes-create.component";
import {RemetenteListComponent} from "./components/remetente/remetente-list/remetente-list.component";
import {RemetenteUpdateComponent} from "./components/remetente/remetente-update/remetente-update.component";


const routes: Routes = [
  {
    path: "remetente/create",
    component: RemetentesCreateComponent
  },
  {
    path: "remetente/list",
    component: RemetenteListComponent
  },
  {
    path: "remetente/update/:id",
    component: RemetenteUpdateComponent
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
