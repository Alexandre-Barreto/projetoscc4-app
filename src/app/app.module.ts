 import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './components/template/header/header.component';

import {MatToolbarModule} from "@angular/material/toolbar";
import { NavComponent } from './components/template/nav/nav.component';
import {MatSidenavModule} from "@angular/material/sidenav";
import {MatListModule} from "@angular/material/list";
import {MatButtonModule} from "@angular/material/button";
import {HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {RemetentesCreateComponent} from "./components/remetente/remetente-create/remetentes-create.component";
import {MatCardModule} from "@angular/material/card";
import { RemetenteListComponent } from './components/remetente/remetente-list/remetente-list.component';
import { ForDirective } from './directive/for.directive';
 import {MatTableModule} from "@angular/material/table";
import { RemetenteList2Component } from './components/remetente/remetente-list2/remetente-list2.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { RemetenteUpdateComponent } from './components/remetente/remetente-update/remetente-update.component';
import {MatSnackBarModule} from "@angular/material/snack-bar";

 @NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    NavComponent,
    RemetentesCreateComponent,
    RemetenteListComponent,
    ForDirective,
    RemetenteList2Component,
    RemetenteUpdateComponent
  ],
     imports: [
         BrowserModule,
         AppRoutingModule,
         BrowserAnimationsModule,
         MatToolbarModule,
         MatSidenavModule,
         MatListModule,
         MatCardModule,
         MatButtonModule,
         HttpClientModule,
         FormsModule,
         MatFormFieldModule,
         MatInputModule,
         MatTableModule,
         MatPaginatorModule,
         MatSortModule,
         MatSnackBarModule
     ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
